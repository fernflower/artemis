[tox]
envlist = {py36,py37}-{static-analysis,unit-tests},type-check

[testenv]
# Use a single env directory for all environments but separate by Python versions.
#
# It could be easier to just say:
#
#   envdir = {toxworkdir}/{basepython}
#
# but, since it's hardcoded in Tox, basepython is not set when envdir is being examined,
# therefore, the replacement fails, hence the explicit names.
envdir =
  py36: {toxinidir}/.tox/py36
  py37: {toxinidir}/.tox/py37

# Don't spoil our nice virtualenvs with system packages
sitepackages = False

# Just test dependencies.
deps = -rtest-requirements.txt

# Pass necessary env vars to let CI and coverage gathering play together nicely
passenv = CI TRAVIS TRAVIS_* IN_TEST

# Conditionals don't support multiline branches, therefore codecov upload is a separate step.
# but that's fine, thanks to options we give it it knows where to find coverage (specific for
# Python version), and we even tell it to add Python version as a flag - keeping coverage
# separate even in Codecov seems to be a good starting point, we can always start merging
# them later.
#
# And there's an advantage: the codecov upload does not run when one runs *-unit-tests env locally.
# It'd fail anyway, since one does not usually have CODECOV_TOKEN set, and it's still possible
# to run it explicitly to upload one's local coverage in case of need.
commands =
  static-analysis: flake8 --config=setup.cfg artemis/ tests/ hooks/
  unit-tests: pytest -v -ra {posargs}

[testenv:type-check]
envdir = {toxinidir}/.tox/type-check
basepython = python3.7
skip_install = False
deps =
  mypy==0.720
  mypy-extensions==0.4.1
  typing-extensions==3.7.4
  sqlalchemy-stubs

commands = mypy --config-file {toxinidir}/mypy.ini --strict --allow-untyped-calls \
             {toxinidir}/artemis \
             {toxinidir}/hooks {posargs}
